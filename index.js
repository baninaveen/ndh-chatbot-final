const express = require("express");
const bodyParser = require("body-parser");
const axios = require("axios");
require("dotenv").config();

const PORT = 4000;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  return res.send(`Hello World Comes from test with pull bit bucket`);
});

app.listen(PORT, () => console.log(`Chatbot is running on ${PORT}`));
